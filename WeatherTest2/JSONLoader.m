//
//  JSONLoader.m
//  JSON parser
//
//  Created by Капитан on 14.07.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "JSONLoader.h"

@implementation JSONLoader

+ (instancetype)initWithURL:(NSURL*)url {
    return [[JSONLoader alloc] initWithURL:url];
}

- (instancetype)initWithURL:(NSURL*)url {
    if (self = [super init]) {
        NSError *respondError;
        NSData *JSONData = [[NSData alloc] initWithContentsOfURL:url];
        if (JSONData) {
            _JSONdataDictionary = [NSJSONSerialization JSONObjectWithData:JSONData
                                                              options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves
                                                                    error:&respondError];
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Loading JSON Error"
                                                                message:@"check your network connection or file existing"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];            
        }
    }
    
    return self;
}

@end
