//
//  ALXAppDelegate.h
//  WeatherTest2
//
//  Created by Капитан on 01.08.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
