//
//  ALXViewController.m
//  WeatherTest2
//
//  Created by Капитан on 01.08.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import "ALXViewController.h"
#import "EDSunriseSet.h"
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import "DayScene.h"
#import "NightScene.h"
#import "JSONLoader.h"

@interface ALXViewController () <CLLocationManagerDelegate>

@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) EDSunriseSet *sunSheddule;
@property (nonatomic, strong) NSString *temperature;
@property (nonatomic, strong) NSString *pressure;
@property (nonatomic, strong) NSString *humidity;
@property (nonatomic, assign) BOOL isDay;
@property (nonatomic, assign) BOOL isBackground;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) DayScene *scene;

@property IBOutlet SKView *skView;
@property (weak, nonatomic) IBOutlet UILabel *sunRiseLabel;
@property (weak, nonatomic) IBOutlet UILabel *sunRise;
@property (weak, nonatomic) IBOutlet UILabel *sunSetLabel;
@property (weak, nonatomic) IBOutlet UILabel *sunSet;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *degree;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabelText;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabelText;
@property (weak, nonatomic) IBOutlet UILabel *presureLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;

@end

@implementation ALXViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getCurrentLocation];
    _isBackground = NO;
    [self getWeatherData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [_locationManager stopUpdatingLocation];
}

- (void)getWeatherData {
    NSURL *url = [NSURL URLWithString:@"http://api.worldweatheronline.com/free/v1/weather.ashx?q=Kiev&format=json&num_of_days=1&key=cd53714c725565c145e4b873d02ffe3ad1d2e19d"];
    JSONLoader *loader = [JSONLoader initWithURL:url];
    NSDictionary *generalData = [loader.JSONdataDictionary objectForKey:@"data"];
    NSArray *currentConditionsArray = [generalData objectForKey:@"current_condition"];
    NSDictionary *weatherConditions = currentConditionsArray[0];
    _temperature = [weatherConditions objectForKey:@"temp_C"];
    _pressure = [weatherConditions objectForKey:@"pressure"];
    _humidity = [weatherConditions objectForKey:@"humidity"];
    _humidity = [_humidity stringByAppendingString:@"%"];
}

# pragma mark - Location

- (void)getCurrentLocation {
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    _currentLocation = [locations lastObject];
//    NSLog(@"_currentLocation latitude %f", _currentLocation.coordinate.latitude);
//    NSLog(@"_currentLocation longitude %f", _currentLocation.coordinate.longitude);
    [self createSunShedule];
    
    [self.locationManager stopUpdatingLocation];
}

# pragma mark - action

- (void)createSunShedule {
    _sunSheddule = [EDSunriseSet sunrisesetWithTimezone:[NSTimeZone localTimeZone]
                                          latitude:_currentLocation.coordinate.latitude
                                         longitude:_currentLocation.coordinate.longitude];
    NSDate *date = [NSDate date];
    [_sunSheddule calculateSunriseSunset:date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *currentTimeComponents = [calendar components:NSHourCalendarUnit|
                                                                   NSMinuteCalendarUnit|
                                                                   NSSecondCalendarUnit
                                                          fromDate:date];
//    NSLog(@"_sunSheddule %@", _sunSheddule.sunrise);
    [self createTimer:currentTimeComponents];
}

- (void)createTimer:(NSDateComponents *)currentTimeComponents {
    if (_timer) {
        [_timer invalidate];
    }
    _timer = nil;
    float currentTimeInSec = currentTimeComponents.hour * 60 * 60 + currentTimeComponents.minute * 60 + currentTimeComponents.second;
    float sunRiseInSec = _sunSheddule.localSunrise.hour * 60 * 60 + _sunSheddule.localSunrise.minute * 60 + _sunSheddule.localSunrise.second;
    float sunSetInSec = _sunSheddule.localSunset.hour * 60 * 60 + _sunSheddule.localSunset.minute * 60 + _sunSheddule.localSunset.second;
    float midNight = 24 * 60 * 60;
    float difference;

    if (currentTimeInSec > sunRiseInSec && currentTimeInSec < sunSetInSec) {
        difference = sunSetInSec - currentTimeInSec;
        _isDay = YES;
    } else if (currentTimeInSec > sunRiseInSec && currentTimeInSec < midNight) {
        difference = midNight - currentTimeInSec + sunRiseInSec;
        _isDay = NO;
    } else {
        difference = sunRiseInSec - currentTimeInSec;
        _isDay = NO;
    }
    [self setTimerShedule:difference];
    
    if (_isBackground == NO) {
        [self setBackgroundImageView:nil];
    }
}

- (void)setTimerShedule:(float)interval {
//    NSLog(@"timer %f", interval);
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
//    interval = 10;
    _timer = [NSTimer scheduledTimerWithTimeInterval:interval
                                              target:self
                                            selector:@selector(setBackgroundImageView:)
                                            userInfo:nil
                                             repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)setBackgroundImageView:(NSTimer *)timer {
    SKView *skView = _skView;
//    DayScene *scene;
    if ( _isDay) {
        _scene = [DayScene sceneWithSize:CGSizeMake(320, 560)];
    } else {
        _scene = [NightScene sceneWithSize:CGSizeMake(320, 560)];
    }
    
    NSString *sunRiseHours = [NSString stringWithFormat:@"%.2ld", (long)_sunSheddule.localSunrise.hour];
    NSString *sunRiseMinutes = [NSString stringWithFormat:@"%.2ld", (long)_sunSheddule.localSunrise.minute];
    NSMutableString *sunRiseTime = [NSMutableString stringWithString: sunRiseHours];
    [sunRiseTime appendString: @":"];
    [sunRiseTime appendString: sunRiseMinutes];
    _sunRiseLabel.text = @"Sunrise:";
    _sunRise.text = sunRiseTime;
    
    NSString *sunSetHours = [NSString stringWithFormat:@"%.2ld", (long)_sunSheddule.localSunset.hour];
    NSString *sunSetMinutes = [NSString stringWithFormat:@"%.2ld", (long)_sunSheddule.localSunset.minute];
    NSMutableString *sunSetTime = [NSMutableString stringWithString: sunSetHours];

    [sunSetTime appendString: @":"];
    [sunSetTime appendString: sunSetMinutes];
    _sunSetLabel.text = @"Sunset:";
    _sunSet.text = sunSetTime;
    _temperatureLabel.text = _temperature;
    _degree.text = @"\u00B0C";
    
    _pressureLabelText.text = @"pressure";
    _humidityLabelText.text = @"humidity";
    _presureLabel.text = _pressure;
    _humidityLabel.text = _humidity;
    
    _scene.scaleMode = SKSceneScaleModeAspectFill;
    [skView presentScene:_scene];
    [self.view addSubview:skView];
    if (_isBackground == NO){
        _isBackground = YES;
    } else {
        [self createSunShedule];
    }
}

- (IBAction)changeSpeed:(id)sender {
//    _scene.duration = _speedSlider.value;
    [_scene createBalloonFollowPathDuration:_speedSlider.value];
//    NSLog(@"_scene.duration %d",_scene.duration);
}


@end
