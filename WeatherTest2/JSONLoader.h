//
//  JSONLoader.h
//  JSON parser
//
//  Created by Капитан on 14.07.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONLoader : NSObject

@property (nonatomic, strong) NSMutableDictionary *JSONdataDictionary;

+ (instancetype)initWithURL:(NSURL*)url;

@end
