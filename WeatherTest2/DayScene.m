#import "DayScene.h"
@import AVFoundation;
#define   DEGREES_TO_RADIANS(degrees) ((M_PI * degrees)/ 180)

@interface DayScene () {
    AVAudioPlayer *_backgroundAudioPlayer;
}

@property (strong) SKAction *sequence;
@property (nonatomic, strong) SKSpriteNode *balloon;

@end

@implementation DayScene




- (id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {

        SKSpriteNode *background = [self addBackgroundImage];
        background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        [self addChild:background];
        _duration = 30;
        [self addBalloon];
        
        _isRainy = YES;
        if (_isRainy) {
            SKEmitterNode *rain = [NSKeyedUnarchiver unarchiveObjectWithFile:[[NSBundle mainBundle]pathForResource:@"Rain" ofType:@"sks"]];
            rain.position = CGPointMake(size.width/2, size.height);
            [rain advanceSimulationTime:10];
            [self addChild:rain];
            [self startBackgroundMusic];
        }
    }
    
    return self;
}

- (void)addBalloon {
    _balloon = [SKSpriteNode spriteNodeWithImageNamed:@"airship"];
    _balloon.position = CGPointMake(-20, 350);
    _balloon.xScale = 0.15;
    _balloon.yScale = 0.15;
    [self addChild:_balloon];
    [self createBalloonFollowPathDuration:_duration];
}

- (void)createBalloonFollowPathDuration:(int)duration {
    [_balloon removeAllActions];
    UIBezierPath *aPath = [self createArcPath];
    SKAction *followCircle = [SKAction followPath:aPath.CGPath asOffset:NO orientToPath:NO duration:duration];
    SKAction *moveLeft  = [SKAction moveToX:-20 duration:0];
    _sequence = [SKAction repeatActionForever:[SKAction sequence:@[followCircle, moveLeft]]];
    [_balloon runAction:_sequence];
}

- (UIBezierPath *)createArcPath {
    UIBezierPath *aPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(160, -10)
                                                         radius:360
                                                     startAngle:DEGREES_TO_RADIANS(130)
                                                       endAngle:DEGREES_TO_RADIANS(47)
                                                      clockwise:NO];
    return aPath;
}


- (void)startBackgroundMusic {
    NSError *err;
    NSURL *file = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"rain.wav" ofType:nil]];
    _backgroundAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:file error:&err];
    if (err) {
        NSLog(@"error in audio play %@",[err userInfo]);
        return;
    }
    [_backgroundAudioPlayer prepareToPlay];
    _backgroundAudioPlayer.numberOfLoops = -1;
    [_backgroundAudioPlayer setVolume:1.0];
    [_backgroundAudioPlayer play];
}

- (SKSpriteNode *)addBackgroundImage {
    SKSpriteNode *backgroundImage = [SKSpriteNode spriteNodeWithImageNamed:@"day"];
    return backgroundImage;
}


-(void)update:(CFTimeInterval)currentTime {

}


@end
