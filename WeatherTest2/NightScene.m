#import "NightScene.h"

@interface NightScene ()

@end


@implementation NightScene

- (SKSpriteNode *)addBackgroundImage {
    SKSpriteNode *backgroundImage = [SKSpriteNode spriteNodeWithImageNamed:@"night"];
    return backgroundImage;
}

@end
