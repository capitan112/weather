#import <SpriteKit/SpriteKit.h>
#import "EDSunriseSet.h"

@interface DayScene : SKScene

@property (nonatomic, assign) BOOL isRainy;
@property (nonatomic, assign) int duration;

- (void)createBalloonFollowPathDuration:(int)duration;

@end
