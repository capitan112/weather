//
//  main.m
//  WeatherTest2
//
//  Created by Капитан on 01.08.14.
//  Copyright (c) 2014 Capitan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ALXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ALXAppDelegate class]));
    }
}
